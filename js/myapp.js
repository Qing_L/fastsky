//document.write("<script language='javascript' src='one.js'></script>");
function getURLParam(name, queryString){
    if (queryString===undefined) {
        queryString = location.search;
    }
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(queryString)||[,""])[1].replace(/\+/g, '%20'))||null;
};

var curSurveyId = null;
var aladin = null;
var curTarget = null;
var cat1 = null;
var cat2 = null;

$(document).ready(function (){
	var surveys = {};

    var survey = getURLParam('survey') || "P/DSS2/color";
    var fov = getURLParam('fov') || 3;
    if (isNaN(fov)) {
        fov = 3;
    }
    
    var defaultTarget = 'M31';
    var target = getURLParam('target') || 'M31';
    
    aladin = A.aladin('#aladin-lite-div',
            {
                survey: survey, // set initial image survey
                fov: 3, // initial field of view in degrees
                target: target, // initial target
                cooFrame: 'J2000', // set galactic frame
                reticleColor: '#ff89ff', // change reticle color
                showGotoControl:false,
			});
    
	$('#menubar li').click(function(){
        	$('#menubar li').each(function(){
        		$(this).removeClass("active");
        	});
        	$(this).addClass("active");
        	if($('#menu-image').hasClass("active"))
        	{
				$("#right > div").hide();
				$("#tabs li").attr("id","");
				$("#tabs li:eq(0)").attr("id","current");
				$($("#current a").attr("name")).fadeIn();
        	}
        	else if($('#menu-catalog-simbad').hasClass("active"))
        	{
        		$("#right > div").hide();
				$("#tabs li").attr("id","");
				$("#tabs li:eq(1)").attr("id","current");
				$($("#current a").attr("name")).fadeIn();
        	}
        	else if($('#menu-catalog-vizier').hasClass("active"))
        	{
        		$("#right > div").hide();
				$("#tabs li").attr("id","");
				$("#tabs li:eq(2)").attr("id","current");
				$($("#current a").attr("name")).fadeIn();
        	}
        });
    
    
    curSurveyId = survey;
    curTarget = target;

    if (target!=defaultTarget) {
        $('#target').val(target);
    }

    $("#target").keypress(function(event) {
        if (event.which == 13) {
            goto();
        }
    });
    
    var display_num=0;
    var votable_num=0;

	$('#simbad_allsky').click(function(){
		if(display_num==0)
		{
			display(0);
			display_num=1;
		}				
		if(display_num==1)
		{
			cat1.show();
			display_num=2;
		}
		else if(display_num==2)
		{
			cat1.hide();
			display_num=1;
		}
		var cats = aladin.view.catalogs;//获取添加的catalos
	});
	
	$('#grid').click(function(){
			if($(this).hasClass("icon-color"))
			{
				aladin.showCooGrid(true);
				$('#grid').removeClass("icon-color");
				$('#grid').addClass("icon-color-select");
			}				
			else
			{
				aladin.showCooGrid(false);
				$('#grid').addClass("icon-color");
				$('#grid').removeClass("icon-color-select");
			}				
		});
	$('#wink').click(function(){
			if($(this).hasClass("icon-color"))
			{
				aladin.showCatalog(true);
				$(this).find("span").removeClass("icon-eye-close");
				$(this).find("span").addClass("icon-eye-open");
				$(this).removeClass("icon-color");
				$(this).addClass("icon-color-select");
			}				
			else
			{
				aladin.showCatalog(false);
				$(this).find("span").addClass("icon-eye-close");
				$(this).find("span").removeClass("icon-eye-open");
				$(this).addClass("icon-color");
				$(this).removeClass("icon-color-select");
			}				
		});
	$('#table-div').hide();
	
	$("#select").click(function (){
		if($(this).hasClass("icon-color"))
			{
				aladin.select();
				$(this).removeClass("icon-color");
				$(this).addClass("icon-color-select");
			}				
			else
			{
				$(this).addClass("icon-color");
				$(this).removeClass("icon-color-select");
			}				
	});
	aladin.on('select', function(sources) {
			var tablehtml = "";
	        var s;
	        for (var k=0; k<sources.length; k++) {
	            s = sources[k];
	            s.select();
	            
				tablehtml = tablehtml+"<tr>"+"<th>"+k+"</th>"
					+"<th>"+'<a target="_blank" href="http://simbad.u-strasbg.fr/simbad/sim-basic?Ident=' 
					+ encodeURIComponent(s.data.MAIN_ID) + '">'+s.data.MAIN_ID+"</th>"
					+"<th>"+s.data.OTYPE+"</th>"+"<th>"+s.data.RA+"</th>"
					+"<th>"+s.data.DEC+"</th>"+"<th>"+s.data.COO_ERR_MAJA+"</th>"
					+"<th>"+s.data.COO_ERR_MINA+"</th>"+"<th>"+s.data.COO_ERR_ANGLE+"</th>"
					+"<th>"+s.data.PMRA+"</th>"+"<th>"+s.data.PMDEC+"</th>"
					+"<th>"+s.data.B+"</th>"+"<th>"+s.data.V+"</th>"+"<th>"+s.data.R+"</th>"
					+"<th>"+s.data.J+"</th>"+"<th>"+s.data.H+"</th>"+"<th>"+s.data.K+"</th>"
					+"<th>"+s.data.SP_TYPE+"</th>"+"<th>"+s.data.GALDIM_MAJAXIS+"</th>"
					+"<th>"+s.data.GALDIM_MINAXIS+"</th>"+"<th>"+s.data.GALDIM_ANGLE+"</th>"
					+"<th>"+s.data.BIBLIST+"</th>"
					+"</tr>";
	        }//for
	        $('#group_one').html(tablehtml);
	        $('#table-div').fadeIn(1000);
	        $('#table-head').stickUp();
	        $("#select").removeClass("icon-color-select");
	        $('#select').addClass("icon-color");
    	});//aladin.on(select)
    	
    $('#share').click(function(){
    	if($(this).hasClass("icon-color"))
    	{
    		var i = new Image();
    		i=aladin.view.getCanvasDataURL();
    		//document.write("<img id=\"img\" src='"+i+"' alt='from canvas'/>");  
    		alert($('img').attr("src"));
    		shareToSina("分享测试","","",$('img').attr("src"));
    		$(this).removeClass("icon-color");
    		$(this).addClass("icon-color-select");
    	}
    		
    });
    
    $("#right > div").hide(); //隐藏所有的#content 里div里所有的内容
	$("#tabs li:first").attr("id","current"); //为第一个li加上id属性，
	$("#right div:first").fadeIn(); //显示#content里第一个div的内容
	$("#tabs a").click(function(e){
		e.preventDefault();
		$("#right > div").hide();
		$("#tabs li").attr("id","");
		$(this).parent().attr("id","current");
		$($(this).attr("name")).fadeIn();
	});
	$(".button-close").click(function(e){
		e.preventDefault();
		$("#right > div").hide();
		$("#tabs li").attr("id","");
		
		$($(this).parent().attr("name")).hide();
		$(this).parent().parent().remove();
		
		$("#tabs li:first").attr("id","current");
		$($("#current a").attr("name")).fadeIn();
	});
});

	function addTableContainer(){
		var containerhtml="";
	        containerhtml = '<div id="container1" class="alert alert-success alert-dismissible" style="margin-top:15px;height:440px;width:auto;" role="alert">'+
			'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
			'<div  class="table-responsive"  >'+
				'<!-- content -->'+
				'<table id="senfe" class="table table-hover table-bordered" >'+
					'<thead><tr>'+
							'<th>#</th><th>main_id</th><th>otype</th><th>RA</th><th>DEC</th><th>COO_ERR_MAJA</th><th>COO_ERR_MINA</th>'+
							'<th>PMRA</th><th>PMDEC</th><th>B</th><th>V</th><th>R</th><th>J</th><th>H</th><th>K</th><th>SP_TYPE</th>'+
							'<th>GALDIM_MAJAXIS</th><th>GALDIM_MINAXIS</th><th>GALDIM_ANGLE</th><th>BIBLIST</th>'+
						'</tr></thead><tbody id="group_one"></tbody></table></div>'+
			'<div style="text-align: right;margin-right: 25px;">'+
				'<a href="#container1" onclick="page.firstPage();">首 页</a>/'+
				'<a href="#container1" onclick="page.nextPage();">下一页</a>/'+
				'<a href="#container1" onclick="page.prePage();">上一页</a>/'+
				'<a href="#container1" onclick="page.lastPage();">末 页</a>'+
				'<span id="divFood"> </span> /第 <input id="pageno" value="1" style="width:20px"/>页/'+
				'<a href="#container1" onclick="page.aimPage();">跳转</a></div> </div>';
		if($('#container1').length==0)
		{
			$('#right').html(containerhtml);
		}
	}

	function addSimbadCatalogPanel()
	{
		var panelhtml="";
		panelhtml = '<div id="catalog-simbad" class="col-md-12">'+
		           		'<h4 style="margin-bottom: -5px;">Simbad Catalog</h4>'+
		           		'<hr class="col-lg-11" style="background-color: #34495e;margin-bottom: 0px;">'+
					    '<div class="col-lg-12" style="width:100%;">'+
						'<h6 class="title col-lg-6">sky area</h6> '+
						'<button id="simbad_allsky" class="btn btn-primary btn col-lg-6" style="margin-top: 5px;">All sky</button>'+
						'<div class="col-lg-12"><!--cone-->'+
						'<input type="radio" name="mode" value="cone" checked="checked" />Cone search'+
						'<div><label>Center: </label>'+
						'<input id="cone_center" type="text" placeholder="position/object name" disabled="disabled" />'+
						'</div><div><label>Radius: </label>'+
						'<input id="cone_radius" type="number" value="5" style="width: 40%;" />'+
						'<select id="cone_select"><option>mas</option><option selected="selected">arcsec</option>'+
						'<option>arcmin</option><option>deg</option></select></div><!--box-->'+
						'<input type="radio" name="mode" value="box" />Box<div><label>Center: </label>'+
						'<input id="box_center" type="text" placeholder="position/object name" disabled="disabled" />'+
						'</div><div><label>Width:  </label><input id="box_width" type="number" value="20" style="width: 40%;" />'+
						'<select id="box_width_select"><option>mas</option><option selected="selected">arcsec</option>'+
						'<option>arcmin</option><option>deg</option></select></div><div>'+
						'<label>Height: </label><input id="box_height" type="number" value="20" style="width: 40%;" />'+
						'<select id="box_height_select"><option>mas</option><option selected="selected">arcsec</option>'+
						'<option>arcmin</option><option>deg</option></select></div><div>'+
						'<label>Angle:  </label><input id="box_angle" type="number" value="0" style="width: 40%;" />'+
						'<select><option>deg</option></select></div></div><div class="col-lg-12"><!--zone-->'+
						'<input type="radio" name="mode" value="zone" />Zone<div><label>αmin: </label>'+
						'<input id="minRA" type="number"  value="350" /><label>αmax: </label>'+
						'<input id="maxRA" type="number" value="20" /></div><div><label>σmin: </label>'+
						'<input id="minDec" type="number" value="-45" /><label>σmax: </label>'+
						'<input id="maxDec" type="number" value="-30" /></div><!--Healpix cell-->'+
						'<input type="radio" name="mode" value="healpix" />Healpix cell(ICRS)<div>'+
						'<label>Nside: </label><select id="nside"><option>1</option><option>2</option>'+
						'<option>4</option><option>8</option><option>16</option><option>32</option>'+
						'<option>64</option><option>128</option><option>256</option>'+
						'<option selected="selected">512</option><option>1024</option><option>2048</option>'+
						'</select></div><div><label>index: </label><input id="ipix" type="number" value="0" />'+
						'</div><!--allsky--><input type="radio" name="mode" value="allsky" />All-sky</div>'+
						'<h6 class="title col-lg-12">Output</h6><div class="col-lg-12"><label>limit: </label>'+
						'<input id="simbad_limit" type="number" value="100" /></div></div>'+
						'<button class="btn btn-primary btn col-lg-6" style="margin-top: 5px;">commit</button></div> <!-- catalog-simbad -->';
		if($('#catalog-simbad'))
		{
			//panelhtml=Template.load("new_file.html");
			$('#right').html(panelhtml);
		}		           	
	}
	
	function addVizieRCatalogPanel(){
		var panelhtml="";
		panelhtml='<div id="catalog-vizier" class="col-md-12">'+
		           	'<h4 style="margin-bottom: -5px;">VizieR Catalog</h4>'+
		           	'<hr class="col-lg-11" style="background-color: #34495e;margin-bottom: 0px;">'+
					'<button id="vizier_btn" class="btn btn-primary col-lg-6" style="margin-top: 5px;">All sky</button>'+
		           	'</div> <!-- catalog-vizier -->';
		if($('#catalog-vizier'))
		{
			$('#right').html(panelhtml);
		}
	}

	function addSurveyImagePanel()
	{
		var panelhtml="";
		panelhtml='<div id="survey" class="col-md-12"><div class="title">Surveys:</div>'+
		           	'<div id="surveys"></div>'+
		           	'</div> <!-- image-survey -->';
		if($('#survey'))
		{
			$('#right').html(panelhtml);
			$.ajax({
         url: "http://aladin.u-strasbg.fr/java/nph-aladin.pl",
         data: {"frame": "aladinLiteDic"},
         method: 'GET',
         dataType: 'jsonp',
         success: function(data) {
             var tooltipDescriptions = {};
             var res = '<div class="row surveys-list">';
             data.sort(function(a,b) { return a.order == b.order ? 0 : a.order < b.order ? -1 : 1; });
             surveys = data;
             for (var k=0; k<data.length; k++) {
                  var id = data[k].id;
                  var w = /^\w+\/(\w+)/.exec(data[k].treePath)[1];
                  var s1 = id.substring(2).replace("/color","");
                  var imgPath = 'img/' + id.replace(/\//g, "_") + '.jpg';
                  
                  res += '<div class="col-md-6 col-xs-12" > <div class="survey col-md-6 col-xs-12" data-surveyId="' + id + '"><div class="survey-label">' + s1 + '</div><img class="survey-preview" src="' + imgPath + '" /><div class="survey-selected" style="display: none;"><div class="survey-selected-img"></div></div></div></div>';
                  tooltipDescriptions[id] = '<div>Band: ' + w + '</div><div>' + data[k].description + '</div>';
             }
             res += '</div>';
             $('#surveys').html(res);
             //alert($('#surveys').html());

             $('.survey').each(function() {
                 $(this).tooltipster({
                     content: $(tooltipDescriptions[$(this).attr('data-surveyId')]),
                     delay: 800,
                     position: 'right'
                 });
             });
             $('.survey').click(function() {
                curSurveyId = $(this).attr('data-surveyId');
                setSurvey(curSurveyId);
                updateHistory();
                $('.survey-selected').hide();
                $(this).find('.survey-selected').show();
             });

             // once the info about surveys retrieved, we can set the info about the current one
             setInfo(curSurveyId);
             var currentSurveyDiv = $('.survey[data-surveyId="' + curSurveyId + '"]');
             currentSurveyDiv.find('.survey-selected').show();
             // scroll to current survey if needed
             var shiftY = currentSurveyDiv.position().top - $('.surveyDiv').position().top;
             if (shiftY>400) {
                $('.surveyDiv').animate({scrollTop: shiftY});
             }
         },
         error: function() { $('#surveys').html("Error: "+url); }
     });
		}
	}

    var top = window.screen.height / 2 - 250;
	var left = window.screen.width / 2 - 300;  
	
	function shareToSina(title,rLink,site,pic) {  
	    title = "分享测试";  
	  // pic = $(".p-img img").attr("src");  
	   rLink = "http://127.0.0.1:8020/AladinLite/index.html";  
	     
	    window.open("http://service.weibo.com/share/share.php?pic=" +encodeURIComponent(pic) +"&title=" +   
	    encodeURIComponent(title.replace(/&nbsp;/g, " ").replace(/<br \/>/g, " ")),  
	    "分享至新浪微博",  
	    "height=500,width=600,top=" + top + ",left=" + left + ",toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no");  
	    
	    $('#share').removeClass("icon-color");
    	$('#share').addClass("icon-color-select");
	}  
	
	function goto() {
       var newTarget = $("#target").val();
       aladin.gotoObject(newTarget);
       // TODO : ne mettre 脿 jour que si le gotoObject est successful
       updateHistory();
       curTarget = newTarget;
       alert(curTarget);
    }

    function updateHistory() {
        if (history && history.replaceState) {
            var target = $('#target').val();
            var fov = aladin.getFov()[0].toFixed(2);
            history.replaceState(null, null, "?target=" + encodeURIComponent(target) + "&fov=" + fov + "&survey=" + encodeURIComponent(curSurveyId));
        }
    }

    function find(s) {
        for (var k=0; k<surveys.length; k++) {
            if (surveys[k].id == s) {
                return surveys[k];
            }
        }
        return null;
    }

    function setInfo(s) {
        var s1 = find(s);
        if (!s1) {
            return;
        }
        $('#content').html(s1.description+" - <a href=\""+s1.copyrightUrl+"\">"+s1.copyright+"</a>");
    }

    function setSurvey(s) {
        aladin.setImageSurvey(s);//点击左边的数据源，设置图层
        setInfo(s);
     }
	function display(type)
     {
     	
     	if(type==0){
     		var pos = curTarget;
			var prog = aladin.createProgressiveCatalog('http://alasky.u-strasbg.fr/cats/SIMBAD', 'J2000', 8, {name: 'Prog. SIMBAD', sourceSize: 8});
    	  	aladin.addCatalog(prog);
    	  	$('#wink').find("span").removeClass("icon-eye-close");
			$('#wink').find("span").addClass("icon-eye-open");
			$('#wink').removeClass("icon-color");
			$('#wink').addClass("icon-color-select");
     	}
     	else if(type==1)
     	{
     		var url = 'http://cdsxmatch.u-strasbg.fr/QueryCat/QueryCat?catName=SIMBAD&'+catalog_mode1;
     		cat1 = A.catalogFromURL(url, {sourceSize:10, color: '#cc99bb', displayLabel: false, labelColumn: 'main_id', labelColor: '#ae4', labelFont: '9px sans-serif'});
     		aladin.addCatalog(cat1);
     		
     	}
     	
     }
     
     function VOTable()
     {
     	var pos = curTarget;
     	/*var url = 'http://vizier.u-strasbg.fr/viz-bin/votable?-source=HIP2&-c='+pos+'&-out.add=_RAJ,_DEJ&-oc.form=dm&-out.meta=DhuL&-out.max=9999&-c.rm=580';
	    cat2 = A.catalogFromURL(url, {sourceSize:12, color: '#f08080'});
	    aladin.addCatalog(cat2);*/
	   	var prog = aladin.createProgressiveCatalog('http://alasky.u-strasbg.fr/cats/VizieR', 'galactic', 8, {name: 'Prog. VizieR', sourceSize: 8});
    	aladin.addCatalog(prog);
     }
$.ajax({
         url: "http://aladin.u-strasbg.fr/java/nph-aladin.pl",
         data: {"frame": "aladinLiteDic"},
         method: 'GET',
         dataType: 'jsonp',
         success: function(data) {
             var tooltipDescriptions = {};
             var res = '<div class="row surveys-list">';
             data.sort(function(a,b) { return a.order == b.order ? 0 : a.order < b.order ? -1 : 1; });
             surveys = data;
             for (var k=0; k<data.length; k++) {
                  var id = data[k].id;
                  var w = /^\w+\/(\w+)/.exec(data[k].treePath)[1];
                  var s1 = id.substring(2).replace("/color","");
                  var imgPath = 'img/' + id.replace(/\//g, "_") + '.jpg';
                  
                  res += '<div class="col-md-6 col-xs-12" > <div class="survey col-md-6 col-xs-12" data-surveyId="' + id + '"><div class="survey-label">' + s1 + '</div><img class="survey-preview" src="' + imgPath + '" /><div class="survey-selected" style="display: none;"><div class="survey-selected-img"></div></div></div></div>';
                  tooltipDescriptions[id] = '<div>Band: ' + w + '</div><div>' + data[k].description + '</div>';
             }
             res += '</div>';
             $('#surveys').html(res);
             //alert($('#surveys').html());

             $('.survey').each(function() {
                 $(this).tooltipster({
                     content: $(tooltipDescriptions[$(this).attr('data-surveyId')]),
                     delay: 800,
                     position: 'right'
                 });
             });
             $('.survey').click(function() {
                curSurveyId = $(this).attr('data-surveyId');
                setSurvey(curSurveyId);
                updateHistory();
                $('.survey-selected').hide();
                $(this).find('.survey-selected').show();
             });

             // once the info about surveys retrieved, we can set the info about the current one
             setInfo(curSurveyId);
             var currentSurveyDiv = $('.survey[data-surveyId="' + curSurveyId + '"]');
             currentSurveyDiv.find('.survey-selected').show();
             // scroll to current survey if needed
             var shiftY = currentSurveyDiv.position().top - $('.surveyDiv').position().top;
             if (shiftY>400) {
                $('.surveyDiv').animate({scrollTop: shiftY});
             }
         },
         error: function() { $('#surveys').html("Error: "+url); }
     });