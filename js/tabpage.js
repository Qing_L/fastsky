$(document).ready(function() {
	$("#content div").hide(); //隐藏所有的#content 里div里所有的内容
	$("#tabs li:first").attr("id", "current"); //为第一个li加上id属性，
	$("#content div:first").fadeIn(); //显示#content里第一个div的内容
	$("#tabs a").click(function(e) {
		e.preventDefault();
		$("#content div").hide();
		$("#tabs li").attr("id", "");
		$(this).parent().attr("id", "current");
		$($(this).attr("name")).fadeIn();
	});
});